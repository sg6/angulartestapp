var myApp = angular.module('myApp', ['ngRoute', 'ngRoute']);

myApp.controller('PersonlistCtrl', ['$scope', '$routeParams', function ($scope, $routeParams) {

    $scope.persons = JSON.parse(localStorage.getItem("angularPersons"));

}]);

myApp.controller('PersonDetailCtrl', ['$scope', '$routeParams', function ($scope, $routeParams) {

    var persons = JSON.parse(localStorage.getItem("angularPersons"));
    $scope.person;

    for(var i = 0; i < persons.length; i++) {
      if(persons[i].id == $routeParams.id) {
        $scope.person = persons[i];
      }
    }

    $scope.update = function() {
      for(var i = 0; i < persons.length; i++) {
        if(persons[i].id == $routeParams.id) {
          persons[i].name = $scope.newPerson.name;
          persons[i].email = $scope.newPerson.email;
          persons[i].tel = $scope.newPerson.tel;

          localStorage.setItem("angularPersons", JSON.stringify(persons));
        }
      }
      alert('changed');
    }

}]);

myApp.config(function ($routeProvider) {

    $routeProvider
        .when('/person/:id', {
            controller: 'PersonDetailCtrl',
            templateUrl: 'views/detail.html'
        })
        .when('/', {
            controller: 'PersonlistCtrl',
            templateUrl: 'views/overview.html'
        })
        .otherwise({
            templateUrl: 'views/404.html'
        });

});
